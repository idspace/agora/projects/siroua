# rules overrides workflow:rules

`rules` overrides `workflow:rules` and doesn't merge with them. `workflow:rules` are evaluated first to create the pipeline or not. Then `rules` are evaluated for each job.

```yaml
workflow:
  rules:
    - if: '$FORCE_GITLAB_CI'
    - if: '$CI_MERGE_REQUEST_IID'
      # REGEX IN VARIABLE
    - if: '$CI_COMMIT_BRANCH =~ $BRANCH_REGEX'

when-with-rules-over-workflow:
  script:
    - echo "testing when with rules over workflow"
  rules:
    - if: '$CI_MERGE_REQUEST_IID'
      when: manual
```

The `when-with-rules-over-workflow:` job will run on MRs only and not on the `BRANCH_REGEX`.
