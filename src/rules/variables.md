# Rules' variables

We can set variables for `rules:if`.

The variable is set on the job if the conditions inside the `rules` are met! This is so powerful since it will allow for dynamic jobs (change jobs based on variables).

```yaml
job:
  variables:
    DEPLOY_VARIABLE: "default-deploy"
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:                              # Override DEPLOY_VARIABLE defined
        DEPLOY_VARIABLE: "deploy-production"  # at the job level.
    - if: $CI_COMMIT_REF_NAME =~ /feature/
      variables:
        IS_A_FEATURE: "true"                  # Define a new variable.
  script:
    - echo "Run script with $DEPLOY_VARIABLE as an argument"
    - echo "Run another script if $IS_A_FEATURE exists"
```

[More in the docs](https://docs.gitlab.com/ee/ci/yaml/#rulesvariables)
