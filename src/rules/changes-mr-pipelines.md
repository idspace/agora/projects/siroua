# rule:changes in MR pipelines

In an MR pipelines `rules:changes` uses `git diff` from the parent refs, an not from the last pushed commit.

```
test:
  ...
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - api/*
    ...
```

If we make changes to `api/*` in the first commit pushed to the MR, the `test` job will be created for all the next commits pushed to the MR even if we don\t make changes to `api/*`.

[More in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/341705)
