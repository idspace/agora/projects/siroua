# !reference and arrays
Up to 14.02 `!reference` is not useful with arrays, outside of `script` tag variants. Since it inserts nested arrays, instead of flattening them.

`script` tags support nested arrays, so they work fine with `!reference`.

[More on this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/322992)
