# `rules:if` does not support job local variables.

`rules:if` does does not access variables from the same job they are declared on.

The following job will run on `master` and `preprod`.

```yaml
variables:
  BRANCH_REGEX: '/staging|preprod|master|demo/'

workflow:
  rules:
    - if: '$FORCE_GITLAB_CI'
    - if: '$CI_MERGE_REQUEST_IID'
      # REGEX IN VARIABLE
    - if: '$CI_COMMIT_BRANCH =~ $BRANCH_REGEX'

rules-override-workflow-with-variables:
  variables:
    BRANCH_REGEX: '/staging|demo/'
  script:
    - echo "testing rules override workflow with variables"
  rules:
    - if: '$CI_COMMIT_BRANCH =~ $BRANCH_REGEX'
```
