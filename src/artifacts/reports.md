# Expose reports artifacts to next jobs
To expose reports artifacts to next jobs we use `artifacts:paths`

```yaml
job:
  artifacts:
    paths:
      - report-name.json
    reports:
```