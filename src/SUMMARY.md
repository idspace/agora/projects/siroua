# Summary

[Introduction](README.md)

- [Rules](rules/README.md)
    - [rules:changes in MR pipelines](rules/changes-mr-pipelines.md)
    - [Rules overrides workflow:rules, and don't merge with them.](rules/overrides-worflow-rules.md)
    - [Local variable support](rules/local-variables-support.md)
    - [rules:variables](rules/variables.md)
- [Cache & Artifacts](artifacts/README.md):
    - [Expose reports artifacts to next jobs](artifacts/reports.md)
- [Services](services/README.md)
    - [Service Intercommunication](services/intercommunication.md)
- [IAC](iac/README.md)
    - [Terraform templates](iac/terraform-templates.md)
- [Pipelines](pipelines/README.md)
    - [Triggers](pipelines/triggers.md)
- [Jobs](jobs/README.md)
    - [Job grouping](jobs/grouping.md)
    - [Non blocking manual jobs](jobs/non-blocking-manual-jobs.md)
    - [Joint commands bug](jobs/joint-commands.md)
- [Variables](variables/README.md)
    - [Nested variables](variables/nested-variables.md)
- [Includes and YAML](includes/README.md)
    - [Include doesn't support anchor scripts](includes/includes-anchor-scripts.md)
