# Terraform templates
There are 3 `gitlab-ci.yml` terraform template file.
1. `Base`: Contains hidden jobs to do different base tasks
2. `Base.Latest`: Same as `Base` but with the latest terraform image
3. `Latest`: Contains 4 jobs that use `Base` to do the underlying tasks