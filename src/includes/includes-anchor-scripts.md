# Include doesn't support anchor scripts

Gitlab CI doesn't play well with script overriding and `includes`

```yaml
include: 
  - local: '.gitlab/ci/frontend.yml'

Client Tests:
  extends: .g-frontend-lint-test
  variables:
    CACHE_COMPRESSION_LEVEL: "fast"
  script:
    - *yarn-lint-script
    - *yarn-test-script
  stage: test
  needs: ["Client Install Deps"]
```

> You can’t use YAML anchors across different YAML files sourced by include. You can only refer to anchors in the same file. To reuse configuration from different YAML files, use !reference tags or the extends keyword.

This makes sense since `include` is a gitlab CI syntax and anchor are yaml syntax. So including a CI file does not imply the inclusion of all the yaml syntax.

I suggest you try to use the [!reference](https://docs.gitlab.com/ee/ci/yaml/#reference-tags) or the [extends](https://docs.gitlab.com/ee/ci/yaml/#extends) keywords.