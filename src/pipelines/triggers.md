# Triggers
- We use `trigger` to define downstream pipeline trigger. When a trigger job starts a downstream pipeline is created. 
- `trigger` is user to create multi-project pipelines. 
- `trigger` can be used in conjunction with a small set of [keywords](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html#trigger-job-configuration-keywords).