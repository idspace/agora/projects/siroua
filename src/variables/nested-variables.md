# Nested Variables

Gitlab started supporting nested variables in `13.10` (under feature-flag).

```
BUILD_ROOT_DIR: '${CI_BUILDS_DIR}'
```

[Docs](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#nested-variable-expansion)
