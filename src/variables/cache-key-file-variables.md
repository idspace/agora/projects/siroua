# cache:key:files does not support variables

Gitlab does not yet support variables in `cache:key:files`.

https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html

This does not work, the cache created uses default as its key and does not take the file into consideration.
```yaml
cache:
  key:
    files:
      - $CI_PROJECT_DIR/cache-key.txt
  paths:
    - $CI_PROJECT_DIR/hello.txt
```