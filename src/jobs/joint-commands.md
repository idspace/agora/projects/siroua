# Joint commands bug

Gitlab CI scripts have a weird bug.
> If multiple commands are combined into one command string, only the last command’s failure or success is reported
[Source](https://docs.gitlab.com/ee/ci/yaml/script.html)

The bug is discussed thoroughly here!(should read)
[Source](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25394)

There is another bug with mono-line commands. Mostly related to the same bug.
[Source](https://forum.gitlab.com/t/gitlab-ci-bash-tests-dont-work/56125)