# Job grouping
Jobs can be grouped by being named the same name with suffix numbers like the following
- `test 1 1`
- `test 2:3`
- `test 3/3`

[More](https://docs.gitlab.com/ee/ci/jobs/#group-jobs-in-a-pipeline)