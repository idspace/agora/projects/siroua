# Non blocking manual jobs
Manual jobs block the pipelines .i.e Pipelines won't have a success status until the manual jobs are run.

To allow pipelines to succeed even if the manual jobs are not run, we should specify `allow_failure` on them.

```yaml
- if: '$CI_MERGE_REQUEST_IID'
  when: manual
  # If this is not specified the pipline will be blocked until
  # the job is run manually
  allow_failure: true 
```
- [Source](https://gitlab.com/gitlab-org/gitlab/-/issues/39534)
- [Another discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/34077)