# Service Intercommunication

`services` [docker links](https://docs.docker.com/network/links/) to connect the running job (build) to the other containers defined in `services`. So the connection is one to many from the build job to the services.

[https://docs.gitlab.com/ee/ci/services/#debug-a-job-locally](Source).

To enable intercommunication between services, we use `FF_NETWORK_PER_BUILD` [feature flag](https://docs.gitlab.com/runner/configuration/feature-flags.html) which replaces the links with [user-defined bridge network](https://docs.gitlab.com/runner/executors/docker.html#create-a-network-for-each-job).

[More here!](https://stackoverflow.com/questions/71699326/unable-to-run-selenium-pytest-on-gitlab-ci/71701869#71701869)